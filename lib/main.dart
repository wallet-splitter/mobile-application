import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';
import 'package:wallet_splitter/Pages/root.dart';
import 'package:wallet_splitter/Utility/googleSignIn.dart';
// ignore: unused_import
import 'package:dcdg/dcdg.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(WalletSplitterApp());
}

class WalletSplitterApp extends StatelessWidget {
  // This widget is the root of your application.
  final ThemeData theme = ThemeData();

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => GoogleSignInProvider(),
      child: MaterialApp(
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [const Locale('en', 'US'), const Locale('id', 'ID')],
        title: 'Wallet Splitter',
        debugShowCheckedModeBanner: false,
        // theme: ThemeData(
        //   primarySwatch: primaryColor,
        //   buttonColor: secondaryColor,
        // ),
        theme: theme.copyWith(
            colorScheme: theme.colorScheme.copyWith(
              primary: primaryColor,
              secondary: secondaryColor,
            ),
            dialogTheme: DialogTheme(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(16))))),
        home: RootPage(),
      ),
    );
  }
}

Map<int, Color> primaryColorSwatch = {
  50: Color.fromRGBO(60, 108, 194, .1),
  100: Color.fromRGBO(60, 108, 194, .2),
  200: Color.fromRGBO(60, 108, 194, .3),
  300: Color.fromRGBO(60, 108, 194, .4),
  400: Color.fromRGBO(60, 108, 194, .5),
  500: Color.fromRGBO(60, 108, 194, .6),
  600: Color.fromRGBO(60, 108, 194, .7),
  700: Color.fromRGBO(60, 108, 194, .8),
  800: Color.fromRGBO(60, 108, 194, .9),
  900: Color.fromRGBO(60, 108, 194, 1),
};

Map<int, Color> secondaryColorSwatch = {
  50: Color.fromRGBO(40, 193, 149, .1),
  100: Color.fromRGBO(40, 193, 149, .2),
  200: Color.fromRGBO(40, 193, 149, .3),
  300: Color.fromRGBO(40, 193, 149, .4),
  400: Color.fromRGBO(40, 193, 149, .5),
  500: Color.fromRGBO(40, 193, 149, .6),
  600: Color.fromRGBO(40, 193, 149, .7),
  700: Color.fromRGBO(40, 193, 149, .8),
  800: Color.fromRGBO(40, 193, 149, .9),
  900: Color.fromRGBO(40, 193, 149, 1),
};

Map<int, Color> errorColorSwatch = {
  50: Color.fromRGBO(186, 70, 92, .1),
  100: Color.fromRGBO(186, 70, 92, .2),
  200: Color.fromRGBO(186, 70, 92, .3),
  300: Color.fromRGBO(186, 70, 92, .4),
  400: Color.fromRGBO(186, 70, 92, .5),
  500: Color.fromRGBO(186, 70, 92, .6),
  600: Color.fromRGBO(186, 70, 92, .7),
  700: Color.fromRGBO(186, 70, 92, .8),
  800: Color.fromRGBO(186, 70, 92, .9),
  900: Color.fromRGBO(186, 70, 92, 1),
};

MaterialColor primaryColor = MaterialColor(0xFF3D6CC2, primaryColorSwatch);
MaterialColor secondaryColor = MaterialColor(0xFF28C195, secondaryColorSwatch);
MaterialColor errorColor = MaterialColor(0xBA465C, errorColorSwatch);
