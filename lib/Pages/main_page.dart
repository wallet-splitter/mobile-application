import 'package:auto_size_text/auto_size_text.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:wallet_splitter/Model/wallet_model.dart';
import '/Model/user_model.dart';
import 'SplitPlan/split_plan.dart';
import '/Pages/Wallet/wallet.dart';
import 'AddTransaction/add_transaction.dart';
import 'History/history.dart';
import '/Pages/home.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  final userFirebase = FirebaseAuth.instance.currentUser;
  UserModel userData = UserModel('', '', '');

  int currentTab = 0;
  final PageStorageBucket bucket = PageStorageBucket();
  late Widget currentPage;

  List<WalletModel> walletList = List.empty();

  // Future<void> getUser(String? email) async {
  //   Map<String, String> requestHeaders = {
  //      'email': email!,
  //    };

  //   final response = await http.post(Uri.parse(BaseURL.getUser), headers: requestHeaders);

  //   print(response.body);
  //   final data = jsonDecode(response.body);

  //   userData = UserModel(data['output']['id'], data['output']['name'], data['output']['email']);
  // }

  @override
  void initState() {
    // getUser(userFirebase!.email);
    currentPage = Home();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      body: PageStorage(
        child: currentPage,
        bucket: bucket,
      ),
      
      // Plus Floating Button
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).colorScheme.secondary,
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (_) => AddTransaction()));
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        shape: CircularNotchedRectangle(),
        notchMargin: 10,
        child: Container(
          height: 60,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: [
                  MaterialButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(40.0),
                      ),
                      minWidth: 25,
                      onPressed: () {
                        setState(() {
                          currentPage = Home();
                          currentTab = 0;
                        });
                      },
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              currentTab == 0
                                  ? Icons.home
                                  : Icons.home_outlined,
                              color: currentTab == 0
                                  ? Theme.of(context).colorScheme.primary
                                  : Color.fromRGBO(153, 153, 153, 1),
                              size: 25,
                            ),
                            Text(
                              "Home",
                              style: TextStyle(
                                color: currentTab == 0
                                    ? Theme.of(context).colorScheme.primary
                                    : Color.fromRGBO(153, 153, 153, 1),
                                fontSize: 12,
                              ),
                            ),
                          ])),
                  MaterialButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(40.0),
                      ),
                      // minWidth: 25,
                      onPressed: () {
                        setState(() {
                          currentPage = Wallet();
                          currentTab = 1;
                        });
                      },
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              currentTab == 1
                                  ? Icons.account_balance_wallet_rounded
                                  : Icons.account_balance_wallet_outlined,
                              color: currentTab == 1
                                  ? Theme.of(context).colorScheme.primary
                                  : Color.fromRGBO(153, 153, 153, 1),
                              size: 25,
                            ),
                            Text(
                              "Wallet",
                              style: TextStyle(
                                color: currentTab == 1
                                    ? Theme.of(context).colorScheme.primary
                                    : Color.fromRGBO(153, 153, 153, 1),
                                fontSize: 12,
                              ),
                            ),
                          ])),
                ],
              ),
              //Right Tab Bar Icons
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  MaterialButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(40.0),
                      ),
                      minWidth: 25,
                      onPressed: () {
                        setState(() {
                          currentPage = SplitPlan(userId: userData.id);
                          currentTab = 2;
                        });
                      },
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              currentTab == 2
                                  ? Icons.double_arrow_rounded
                                  : Icons.double_arrow_outlined,
                              color: currentTab == 2
                                  ? Theme.of(context).colorScheme.primary
                                  : Color.fromRGBO(153, 153, 153, 1),
                              size: 25,
                            ),
                            AutoSizeText(
                              "SplitPlan",
                              style: TextStyle(
                                color: currentTab == 2
                                    ? Theme.of(context).colorScheme.primary
                                    : Color.fromRGBO(153, 153, 153, 1),
                                fontSize: 12,
                              ),
                            ),
                          ])),
                  MaterialButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(40.0),
                      ),
                      minWidth: 25,
                      onPressed: () {
                        setState(() {
                          currentPage = History();
                          currentTab = 3;
                        });
                      },
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.history,
                              color: currentTab == 3
                                  ? Theme.of(context).colorScheme.primary
                                  : Color.fromRGBO(153, 153, 153, 1),
                              size: 25,
                            ),
                            AutoSizeText(
                              "History",
                              style: TextStyle(
                                color: currentTab == 3
                                    ? Theme.of(context).colorScheme.primary
                                    : Color.fromRGBO(153, 153, 153, 1),
                                fontSize: 12,
                              ),
                            ),
                          ])),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
