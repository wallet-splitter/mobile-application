import 'package:flutter/material.dart';
import 'create_split_plan.dart';

class NoSplitPlan extends StatefulWidget {
  const NoSplitPlan({
    Key? key,
    required this.userId
  }) : super(key: key);

  final String userId;

  @override
  State<NoSplitPlan> createState() => _NoSplitPlanState();
}

class _NoSplitPlanState extends State<NoSplitPlan> {

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Align(
            alignment: Alignment.centerLeft,
            child: Text(
              "Split Plan",
              style: TextStyle(
                fontSize: 28,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Spacer(),
          Image.asset(
            'images/projection_onboarding.png',
            height: 200,
          ),
          SizedBox(
            height: 65,
          ),
          Text(
            'There\'s no split plan',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 22,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(
            height: 25,
          ),
          Text(
            'You haven’t created any split plan.\nPlease create a new one.',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 12,
              color: Colors.grey,
            ),
          ),
          SizedBox(
            height: 30,
          ),
          TextButton(
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(
                    builder: (_) => CreateSplitPlan(userId: widget.userId,),
                  ));
            },
            child: Text(
              'Create Plan',
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
            ),
            style: ButtonStyle(
                padding: MaterialStateProperty.all<EdgeInsets>(
                    EdgeInsets.only(
                        top: 20, bottom: 20, right: 30, left: 30)),
                foregroundColor:
                    MaterialStateProperty.all<Color>(Colors.white),
                backgroundColor: MaterialStateProperty.all<Color>(
                    Theme.of(context).colorScheme.secondary),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  // side: BorderSide(color: Theme.of(context).colorScheme.secondary, width: 3),
                ))),
          ),
          Spacer(),
        ],
      ),
    );
  }
}
