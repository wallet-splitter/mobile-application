import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:wallet_splitter/Model/wallet_splitplan_model.dart';
import 'package:wallet_splitter/Utility/base_url.dart';
import '../Wallet/wallet_card.dart';
import 'package:http/http.dart' as http;

class EditSplitPlanPrecentage extends StatefulWidget {
  const EditSplitPlanPrecentage({Key? key, required this.wallets}) : super(key: key);

  final List<WalletSplitPlanModel> wallets;

  @override
  _EditSplitPlanPrecentageState createState() => _EditSplitPlanPrecentageState();
}

class _EditSplitPlanPrecentageState extends State<EditSplitPlanPrecentage> {

  List<WalletSplitPlanModel> selectedWallets = [];
  List<Widget> listCardWallet = [];

  Future<int> editSplitPlan(List<WalletSplitPlanModel> selectedWallets) async {
    Map<String, String> requestHeaders = {
      'user-id': selectedWallets[0].userId,
    };

    List<Map> sw = <Map>[];
    
    for(WalletSplitPlanModel wallet in selectedWallets){
      Map map = {
        'wallet_id': wallet.id,
        'percentage': wallet.percentage
      };
      sw.add(map);
    }

    final response = await http.post(
      Uri.parse(BaseURL.editSplitPlan),
      headers: requestHeaders,
      body: jsonEncode(<String, dynamic>{
        'selected_wallets': sw
      }),
    );

    print(response.body);
    final data = jsonDecode(response.body);

    return data['error_schema']['error_code'];
  }

  Future<int> resetSplitPlan(String userId) async {
    Map<String, String> requestHeaders = {
      'user-id': userId,
    };

    final response = await http.post(
      Uri.parse(BaseURL.resetSplitPlan),
      headers: requestHeaders
    );

    print(response.body);
    final data = jsonDecode(response.body);

    return data['error_schema']['error_code'];
  }
  
  @override
  void initState() {
    setState(() {
      selectedWallets = widget.wallets;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    
    listCardWallet = List.generate(widget.wallets.length, (index) => WalletPrecentage(
              wallet: widget.wallets[index],
              onChanged: (double newValue) {
                setState(() {
                  selectedWallets[index].percentage = newValue;
                });
                print('Wallet ke-$index');
                print('Value = ${selectedWallets[index].percentage}');
              },
            ));

    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Plan'),
        titleTextStyle: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.bold,
          fontSize: 22,
        ),
        backgroundColor: Colors.white10,
        elevation: 0,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        actions: [
          TextButton(
            child: Text('Save'),
            onPressed: () async {
              double selectedPercent = 0;

              for (WalletSplitPlanModel w in selectedWallets){
                selectedPercent += w.percentage;
              }

              if(selectedPercent<100 || selectedPercent>100){
                final snackBar = SnackBar(
                  content: const Text(
                    'Total percentage must be 100%!',
                    textAlign: TextAlign.center,
                  ),
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                int tryEdit = await editSplitPlan(selectedWallets).then((value) => value);
                if(tryEdit==0){
                  Navigator.pop(context);
                  Navigator.pop(context);
                } else if (tryEdit==1){
                  final snackBar = SnackBar(
                    content: const Text(
                      'Failed to add split plan!',
                      textAlign: TextAlign.center,
                    ),
                  );
                  ScaffoldMessenger.of(context).showSnackBar(snackBar);
                }
              }
            },
            style: TextButton.styleFrom(
                primary: Theme.of(context).colorScheme.primary,
                textStyle: TextStyle(
                  fontSize: 18,
                ),
                shape: CircleBorder()),
          ),
        ],
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 50),
                  child: Text(
                    'Input wallet percentage to be split from your income',
                    style: TextStyle(fontSize: 22),
                    textAlign: TextAlign.center,
                  ),
                ),
                Column(
                  children: listCardWallet,
                ),
                SizedBox(height: 60,),
                Container(
                  width: 180,
                  height: 45,
                  child: TextButton(
                      style: ButtonStyle(
                        foregroundColor: MaterialStateProperty.all<Color>(
                          Theme.of(context).colorScheme.error,
                        ),
                        backgroundColor:
                            MaterialStateProperty.all<Color>(Colors.transparent),
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                side: BorderSide(color: Theme.of(context).colorScheme.error))),
                      ),
                      onPressed: () async {
                        int tryReset = await resetSplitPlan(widget.wallets[0].userId).then((value) => value);
                        if(tryReset==0){
                          Navigator.pop(context);
                          Navigator.pop(context);
                        } else if (tryReset==1){
                          final snackBar = SnackBar(
                            content: const Text(
                              'Failed to reset split plan!',
                              textAlign: TextAlign.center,
                            ),
                          );
                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        }
                      },
                      child: Text(
                        'Reset Split Plan',
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}