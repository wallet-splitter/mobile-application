import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:wallet_splitter/Model/wallet_splitplan_model.dart';
import 'package:wallet_splitter/Utility/base_url.dart';
import 'view_split_plan.dart';
import 'package:http/http.dart' as http;

import 'no_split_plan.dart';

class SplitPlan extends StatefulWidget {
  const SplitPlan({Key? key, required this.userId}) : super(key: key);

  final String userId;

  @override
  _SplitPlanState createState() => _SplitPlanState();
}

class _SplitPlanState extends State<SplitPlan> {
  final user = FirebaseAuth.instance.currentUser;
  
  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentPage = Center(child: CircularProgressIndicator(),);

  List<WalletSplitPlanModel> walletList = [];

  Future<void> getSplitPlan(String? email) async {
    Map<String, String> requestHeadersUser = {
       'email': email!,
     };

    final responseUser = await http.post(Uri.parse(BaseURL.getUser), headers: requestHeadersUser);
    final dataUser = jsonDecode(responseUser.body);

    String userId = dataUser['output']['id'];

    Map<String, String> requestHeaders = {
      'user-id': userId,
    };

    final response =
        await http.get(Uri.parse(BaseURL.getSplitPlan), headers: requestHeaders);

    print(response.body);
    final data = jsonDecode(response.body);

    if(data['error_schema']['error_code']==0){
      List wallets = data['output'];

      List<WalletSplitPlanModel> tempList = <WalletSplitPlanModel>[];

      for (int i = 0; i < wallets.length; i++) {
        WalletSplitPlanModel walletModel = WalletSplitPlanModel(
            wallets[i]['id'],
            wallets[i]['user_id'],
            wallets[i]['icon'],
            wallets[i]['icon_family'],
            wallets[i]['name'],
            wallets[i]['is_split_plan'],
            wallets[i]['percent_split_plan']);
        tempList.add(walletModel);
      }

      walletList = tempList;

      setState(() {
        currentPage = ViewSplitPlan(wallets: tempList);
      });
    } else if(data['error_schema']['error_code']==1){
      setState(() {
        currentPage = NoSplitPlan(userId: userId,);
      });
    }
  }

  @override
  void initState() {
    getSplitPlan(user!.email);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        top: true,
        bottom: true,
        child: RefreshIndicator(
          onRefresh: (){
            return getSplitPlan(user!.email);
          },
          child: PageStorage(
            bucket: bucket,
            child: currentPage,
          ),
        ),
      ),
    );
  }
}

