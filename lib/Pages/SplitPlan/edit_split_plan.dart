import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:wallet_splitter/Model/wallet_splitplan_model.dart';
import 'package:wallet_splitter/Utility/base_url.dart';
import '../Wallet/wallet_card.dart';
import 'package:http/http.dart' as http;

import 'edit_split_plan_precentage.dart';

class EditSplitPlan extends StatefulWidget {
  const EditSplitPlan({Key? key, required this.userId}) : super(key: key);

  final String userId;

  @override
  _EditSplitPlanState createState() => _EditSplitPlanState();
}

class _EditSplitPlanState extends State<EditSplitPlan> {
  List<bool> _isSelected = List.generate(10, (index) => false);
  List<WalletSplitPlanModel> selectedWallet = [];

  List<WalletSplitPlanModel> walletList = List.empty();
  List<Widget> listCardWallet = List.empty();

  Future<void> getWallets(String userId) async {
    Map<String, String> requestHeaders = {
      'user-id': userId.toString(),
    };

    final response =
        await http.get(Uri.parse(BaseURL.getWallets), headers: requestHeaders);

    print(response.body);
    final data = jsonDecode(response.body);

    List wallets = data['output'];

    List<WalletSplitPlanModel> tempList = <WalletSplitPlanModel>[];

    for (int i = 0; i < wallets.length; i++) {
      WalletSplitPlanModel walletModel = WalletSplitPlanModel(
          wallets[i]['id'],
          wallets[i]['user_id'],
          wallets[i]['icon'],
          wallets[i]['icon_family'],
          wallets[i]['name'],
          wallets[i]['is_split_plan'],
          wallets[i]['percent_split_plan'],);

      tempList.add(walletModel);
      
      if(wallets[i]['is_split_plan']=='1'){
        _isSelected[i] = true;
      }
    }

    setState(() {
      walletList = tempList;
    });
  }

  Future<int> resetSplitPlan(String userId) async {
    Map<String, String> requestHeaders = {
      'user-id': userId,
    };

    final response = await http.post(
      Uri.parse(BaseURL.resetSplitPlan),
      headers: requestHeaders
    );

    print(response.body);
    final data = jsonDecode(response.body);

    return data['error_schema']['error_code'];
  }

  @override
  void initState() {
    getWallets(widget.userId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    List<Widget> listCardWallet = List.generate(
        walletList.length,
        (index) => WalletCheckbox(
              wallet: walletList[index],
              value: _isSelected[index],
              onChanged: (bool newValue) {
                setState(() {
                  _isSelected[index] = newValue;
                });
                print('Wallet ke-$index');
                print('Bool _isSelected = $_isSelected');
              },
            ));

    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Plan'),
        titleTextStyle: TextStyle(
          color: Colors.black,
          fontWeight: FontWeight.bold,
          fontSize: 22,
        ),
        backgroundColor: Colors.white10,
        elevation: 0,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        actions: [
          TextButton(
            child: Text('Next'),
            onPressed: () {
              List<int> selectedIndexWallet = [];

              for (var i = 0; i < _isSelected.length; i++) {
                if (_isSelected[i] == true) selectedIndexWallet.add(i);
              }

              selectedWallet = List.generate(
                  selectedIndexWallet.length,
                  (index) => WalletSplitPlanModel(
                    walletList[selectedIndexWallet[index]].id,
                    walletList[selectedIndexWallet[index]].userId,
                    walletList[selectedIndexWallet[index]].icon.toString(),
                    walletList[selectedIndexWallet[index]].iconFam,
                      walletList[selectedIndexWallet[index]].name,
                      walletList[selectedIndexWallet[index]].isSplitPlan.toString(),
                      walletList[selectedIndexWallet[index]].percentage.toString()));
              
              if(selectedWallet.length<2){
                final snackBar = SnackBar(
                  content: const Text(
                    'Please select minimum of 2 wallets!',
                    textAlign: TextAlign.center,
                  ),
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => EditSplitPlanPrecentage(wallets: selectedWallet,),
                    ));
              }
            },
            style: TextButton.styleFrom(
                primary: Theme.of(context).colorScheme.primary,
                textStyle: TextStyle(
                  fontSize: 18,
                ),
                shape: CircleBorder()),
          ),
        ],
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 20, bottom: 50),
                  child: Text(
                    'Select wallet that you want to include in your split plan',
                    style: TextStyle(fontSize: 22),
                    textAlign: TextAlign.center,
                  ),
                ),
                Column(
                  children: listCardWallet,
                ),
                SizedBox(height: 60,),
                Container(
                  width: 180,
                  height: 45,
                  child: TextButton(
                      style: ButtonStyle(
                        foregroundColor: MaterialStateProperty.all<Color>(
                          Theme.of(context).colorScheme.error,
                        ),
                        backgroundColor:
                            MaterialStateProperty.all<Color>(Colors.transparent),
                        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                side: BorderSide(color: Theme.of(context).colorScheme.error, width: 3))),
                      ),
                      onPressed: () async {
                        int tryReset = await resetSplitPlan(widget.userId).then((value) => value);
                        if(tryReset==0){
                          Navigator.pop(context);
                        } else if (tryReset==1){
                          final snackBar = SnackBar(
                            content: const Text(
                              'Failed to reset split plan!',
                              textAlign: TextAlign.center,
                            ),
                          );
                          ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        }
                      },
                      child: Text(
                        'Reset Split Plan',
                        style: TextStyle(fontSize: 18),
                      ),
                    ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}