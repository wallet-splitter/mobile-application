import 'package:flutter/material.dart';
import 'package:wallet_splitter/Model/wallet_splitplan_model.dart';
import '../Wallet/wallet_card.dart';
import 'edit_split_plan.dart';

class ViewSplitPlan extends StatefulWidget {
  const ViewSplitPlan(
      {Key? key, required this.wallets})
      : super(key: key);

  final List<WalletSplitPlanModel> wallets;

  @override
  _ViewSplitPlanState createState() => _ViewSplitPlanState();
}

class _ViewSplitPlanState extends State<ViewSplitPlan> {

  @override
  Widget build(BuildContext context) {
    List<Widget> listCardWallet = List.generate(
        this.widget.wallets.length,
        (index) => WalletPercenatgeView(
              wallet: this.widget.wallets[index]
            ));

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white10,
        elevation: 0,
        centerTitle: false,
        title: Padding(
          padding: const EdgeInsets.only(left: 5),
          child: Text(
            "Split Plan",
            style: TextStyle(
              color: Colors.black,
              fontSize: 34,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: TextButton(
              child: Text('Edit'),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (_) => EditSplitPlan(
                        userId: widget.wallets[0].userId,
                      ),
                    ));
              },
              style: TextButton.styleFrom(
                primary: Theme.of(context).colorScheme.primary,
                textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  height: 45,
                ),
                Column(
                  children: listCardWallet,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
