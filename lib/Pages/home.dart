import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '/Model/user_model.dart';
import '/Model/wallet_model.dart';
import '/Pages/profile/profile.dart';
import '/Utility/base_url.dart';
import 'package:http/http.dart' as http;

import 'Wallet/wallet_card.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final user = FirebaseAuth.instance.currentUser;
  
  List<bool> _isFavorited = List.generate(10, (index) => false);
  List<Widget> favWalletList = <Widget>[];
  double balance = 0;
  ValueNotifier<UserModel> userData =
      ValueNotifier<UserModel>(UserModel('', '', ''));

  //String? null safety
  Future<void> getFavWallets() async {
    Map<String, String> requestHeadersUser = {
      'email': user!.email!,
    };

    final responseUser = await http.post(Uri.parse(BaseURL.getUser),
        headers: requestHeadersUser);

    print(responseUser.body);
    final dataUser = jsonDecode(responseUser.body);
    userData.value = UserModel(dataUser['output']['id'],
        dataUser['output']['name'], dataUser['output']['email']);
    Map<String, String> requestHeaders;
    requestHeaders = {'user-id': dataUser['output']['id']};

    final response =
        await http.get(Uri.parse(BaseURL.getWallets), headers: requestHeaders);

    print(response.body);
    final data = jsonDecode(response.body);

    List wallets = data['output'];

    //tempList nampung favorite wallet
    List<Widget> tempList = <Widget>[];
    double tempBalance = 0;

    for (int i = 0; i < wallets.length; i++) {
      if (wallets[i]['is_favorite'] == '1') {
        WalletModel walletModel = WalletModel(
            wallets[i]['id'],
            wallets[i]['user_id'],
            wallets[i]['icon'],
            wallets[i]['icon_family'],
            wallets[i]['name'],
            wallets[i]['balance'],
            wallets[i]['is_main']);
        
        if(wallets[i]['is_favorite']=='1'){
          _isFavorited[i] = true;
        }
        
        tempList.add(WalletCard(
          walletModel: walletModel,
          value: _isFavorited[i],
          onChanged: (bool newValue) async {
            setState(() {
              _isFavorited[i] = newValue;
            });
            print('Wallet ke-$i');
            print('Bool _isFavorited = ${_isFavorited[i]}');

            int tryUpdateFav = await updateFav(wallets[i]['user_id'], wallets[i]['id'], _isFavorited[i]).then((value) => value);
            if (tryUpdateFav==1){
              final snackBar = SnackBar(
                content: const Text(
                  'Failed to update favorite wallet!',
                  textAlign: TextAlign.center,
                ),
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            }
          },
        ));
      }

      tempBalance += double.parse(wallets[i]['balance']);
    }

    setState(() {
      favWalletList = tempList;
      balance = tempBalance;
    });
  }

  Future<int> updateFav(String userId, String walletId, bool isFav) async {
    String fav = '0';

    if(isFav)
      fav='1';

    print('$userId, $walletId, $fav');

    Map<String, String> requestHeaders = {
      'user-id': userId,
      'wallet-id': walletId,
      'is-favorite': fav,
    };

    final response = await http.post(
      Uri.parse(BaseURL.updateFavWallets),
      headers: requestHeaders
    );

    print(response.body);
    final data = jsonDecode(response.body);

    return data['error_schema']['error_code'];
  }

  @override
  void initState() {
    getFavWallets();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var currency =
        NumberFormat.currency(locale: "id_ID", symbol: "Rp ", decimalDigits: 0);
    return Scaffold(
      body: SafeArea(
        top: true,
        bottom: true,
        child: RefreshIndicator(
          onRefresh: () async {
            setState(() {
              getFavWallets();
            });
          },
          child: Padding(
            padding: const EdgeInsets.only(top: 20, bottom: 20),
            child: ListView(
              children: [
                Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(20),
                      child: Row(
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (_) => Profile(
                                      userData: userData.value,
                                    ),
                                  ));
                            },
                            child:
                                CircleAvatar(
                              backgroundImage:
                                  NetworkImage(user!.photoURL.toString()),
                              backgroundColor: Colors.black26,
                              radius: 35,
                            ),
                          ),
                          // Text
                          Padding(
                            padding: const EdgeInsets.all(20),
                            child: ValueListenableBuilder(
                                valueListenable: userData,
                                builder: (BuildContext context, UserModel value,
                                    Widget? child) {
                                  return RichText(
                                      text: TextSpan(
                                          text: "Halo, ",
                                          style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black,
                                          ),
                                          children: <TextSpan>[
                                        TextSpan(
                                            text: value.name,
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18,
                                              fontWeight: FontWeight.normal,
                                            ))
                                      ]));
                                }),
                          )
                        ],
                      ),
                    ),

                    // Balance
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 27, bottom: 27, left: 20, right: 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: RichText(
                          text: TextSpan(
                            text: 'Balance: ',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 24,
                              color: Colors.black,
                            ),
                            children: <TextSpan>[
                              TextSpan(
                                  text: currency.format(balance),
                                  style: TextStyle(
                                    fontSize: 24,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  )),
                            ],
                          ),
                        ),
                      ),
                    ),

                    // Text Favourite Wallet
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text("Favourite Wallet",
                            style: TextStyle(fontSize: 18)),
                      ),
                    ),

                    // Column Wallet
                    Column(
                        //ValueListenableBuilder -> baca update dari value taruh di valueListenable
                        children: favWalletList)
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
