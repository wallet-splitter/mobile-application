import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:wallet_splitter/Model/transaction_model.dart';
import '/Pages/History/transaction_detail.dart';
import '/Utility/base_url.dart';
import 'package:http/http.dart' as http;

class History extends StatefulWidget {
  const History({Key? key}) : super(key: key);

  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  final user = FirebaseAuth.instance.currentUser;
  ValueNotifier<List<Widget>> historyList = ValueNotifier<List<Widget>>([]);
  double balance = 0;

  Future<void> getBalance(String? email) async {
    Map<String, String> requestHeadersUser = {
       'email': email!,
     };

    final responseUser = await http.post(Uri.parse(BaseURL.getUser), headers: requestHeadersUser);
    final dataUser = jsonDecode(responseUser.body);

    String userId = dataUser['output']['id'];

    Map<String, String> requestHeaders = {
      'user-id': userId,
    };

    final response =
        await http.get(Uri.parse(BaseURL.getBalance), headers: requestHeaders);

    print(response.body);
    final data = jsonDecode(response.body);

    setState(() {
      balance = double.parse(data['output']);
    });
  }

  Future<void> getHistory(String? email) async {
    Map<String, String> requestHeadersUser = {
       'email': email!,
     };

    final responseUser = await http.post(Uri.parse(BaseURL.getUser), headers: requestHeadersUser);
    final dataUser = jsonDecode(responseUser.body);

    String userId = dataUser['output']['id'];

    Map<String, String> requestHeaders = {
      'user-id': userId.toString(),
    };

    final response =
        await http.post(Uri.parse(BaseURL.getHistory), headers: requestHeaders);

    print(response.body);
    final data = jsonDecode(response.body);
    List<Widget> tempList = <Widget>[];

    if (data['error_schema']['error_code'] == 0) {
      List history = data['output'];

      for (int i = 0; i < history.length; i++) {
        TransactionModel trxModel = TransactionModel(
            history[i]['id'],
            history[i]['user_id'],
            history[i]['wallet_from_id'],
            history[i]['wallet_to_id'],
            history[i]['amount'],
            history[i]['description'],
            history[i]['date'],
            history[i]['is_income'],
            history[i]['is_expense'],
            history[i]['is_transfer']);
        tempList.add(TransactionDetail(
          trxModel: trxModel,
        ));
      }

      historyList.value = tempList;
    } else if (data['error_schema']['error_code'] == 1 && data['output'] == 'History is empty') {
      Widget noHistoryWidget = Align(
        alignment: Alignment.center,
        child: Text('No History', style: TextStyle(fontSize: 18),),
      );

      tempList.add(noHistoryWidget);
      historyList.value = tempList;
    }
  }

  @override
  void initState() {
    getBalance(user!.email);
    getHistory(user!.email);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var currency =
        NumberFormat.currency(locale: "id_ID", symbol: "Rp ", decimalDigits: 0);
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: const EdgeInsets.only(left: 5),
          child: Text(
            'History',
            style: TextStyle(
              fontSize: 34,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
        ),
        centerTitle: false,
        backgroundColor: Colors.white10,
        elevation: 0,
        // actions: [
        //   Padding(
        //     padding: const EdgeInsets.only(right: 10),
        //     child: IconButton(
        //       icon: Icon(
        //         FontAwesomeIcons.slidersH,
        //         color: Colors.black,
        //       ),
        //       onPressed: () {
        //         showModalBottomSheet(
        //             shape: RoundedRectangleBorder(
        //                 borderRadius:
        //                     BorderRadius.vertical(top: Radius.circular(25.0))),
        //             backgroundColor: Colors.white,
        //             context: context,
        //             isScrollControlled: true,
        //             builder: (context) => Padding(
        //                   padding: const EdgeInsets.symmetric(horizontal: 18),
        //                   child: Column(
        //                     crossAxisAlignment: CrossAxisAlignment.start,
        //                     mainAxisSize: MainAxisSize.min,
        //                     children: [
        //                       Padding(
        //                         padding: const EdgeInsets.only(top: 20),
        //                         child: Align(
        //                           alignment: Alignment.center,
        //                           child: Container(
        //                             height: 5,
        //                             width: 85,
        //                             decoration: BoxDecoration(
        //                               borderRadius: BorderRadius.circular(10),
        //                               color: Colors.black,
        //                             ),
        //                           ),
        //                         ),
        //                       ),
        //                       Padding(
        //                         padding:
        //                             const EdgeInsets.symmetric(vertical: 40.0),
        //                         child: Column(
        //                           children: [
        //                             createFilterList('Last Month'),
        //                             Divider(
        //                               thickness: 2.0,
        //                               height: 6.0,
        //                             ),
        //                             createFilterList('Last Three Month'),
        //                             Divider(
        //                               thickness: 2.0,
        //                               height: 6.0,
        //                             ),
        //                             createFilterList('Last Six Month'),
        //                             Divider(
        //                               thickness: 2.0,
        //                               height: 6.0,
        //                             ),
        //                             createFilterList('Custom')
        //                           ],
        //                         ),
        //                       )
        //                     ],
        //                   ),
        //                 ));
        //       },
        //     ),
        //   )
        // ],
      ),
      body: SafeArea(
        top: true,
        bottom: true,
        child: RefreshIndicator(
          onRefresh: () async {
            getBalance(user!.email);
            getHistory(user!.email);
          },
          child: ListView(padding: EdgeInsets.all(20.0), children: [
            Padding(
              padding: const EdgeInsets.only(top: 40.0),
              child: Align(
                alignment: Alignment.center,
                child: RichText(
                  text: TextSpan(
                    text: 'Balance: ',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 24,
                      color: Colors.black,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                          text: currency.format(balance),
                          style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          )),
                    ],
                  ),
                ),
              ),
            ),
            // Padding(
            //     padding: const EdgeInsets.only(top: 23.0, bottom: 16.0),
            //     child: Text(
            //       'This Month',
            //       style: TextStyle(fontSize: 18.0),
            //     )),
            SizedBox(height: 40,),
            ValueListenableBuilder(
              valueListenable: historyList,
              builder:
                  (BuildContext context, List<Widget> value, Widget? child) {
                return Column(
                  children: value,
                );
              },
            )
          ]),
        ),
      ),
    );
  }

  Container createFilterList(String filterName) {
    return Container(
      width: double.infinity,
      child: TextButton(onPressed: () {}, child: Text(filterName)),
    );
  }
}
