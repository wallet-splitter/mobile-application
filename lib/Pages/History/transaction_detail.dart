import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:wallet_splitter/Model/transaction_model.dart';
import 'package:wallet_splitter/Model/wallet_model.dart';
import 'package:wallet_splitter/Utility/base_url.dart';
import '/Utility/icon_code.dart' as iconCode;
import 'package:http/http.dart' as http;

class TransactionDetail extends StatefulWidget {
  const TransactionDetail({Key? key, required this.trxModel}) : super(key: key);

  final TransactionModel trxModel;

  @override
  _TransactionDetailState createState() => _TransactionDetailState();
}

class _TransactionDetailState extends State<TransactionDetail> {
  WalletModel walletData = WalletModel('', '', '0', '', '', '0', '0');
  WalletModel walletDataToTransfer = WalletModel('', '', '0', '', '', '0', '0');

  Future<void> getWalletDetail(String userId, String walletId) async {
    Map<String, String> requestHeaders = {
      'user-id': userId,
      'wallet-id': walletId
    };

    final response = await http.get(Uri.parse(BaseURL.getWalletSingle),
        headers: requestHeaders);

    print(response.body);
    final data = jsonDecode(response.body);

    if (data['error_schema']['error_code'] == 0) {
      Map wallets = data['output'];

      setState(() {
        walletData = WalletModel(
            wallets['id'],
            wallets['user_id'],
            wallets['icon'],
            wallets['icon_family'],
            wallets['name'],
            wallets['balance'],
            wallets['is_main']);
      });
    }
  }

  Future<void> getWalletTo(String userId, String walletId) async {
    Map<String, String> requestHeaders = {
      'user-id': userId,
      'wallet-id': walletId
    };

    final response = await http.get(Uri.parse(BaseURL.getWalletSingle),
        headers: requestHeaders);

    print(response.body);
    final data = jsonDecode(response.body);

    if (data['error_schema']['error_code'] == 0) {
      Map wallets = data['output'];

      setState(() {
        walletDataToTransfer = WalletModel(
            wallets['id'],
            wallets['user_id'],
            wallets['icon'],
            wallets['icon_family'],
            wallets['name'],
            wallets['balance'],
            wallets['is_main']);
      });
    }
  }

  @override
  void initState() {
    if (widget.trxModel.isIncome == 1) {
      setState(() {
        getWalletDetail(widget.trxModel.userId, widget.trxModel.walletToId);
      });
    } else if (widget.trxModel.isExpense == 1) {
      setState(() {
        getWalletDetail(widget.trxModel.userId, widget.trxModel.walletFromId);
      });
    } else if (widget.trxModel.isTransfer == 1) {
      setState(() {
        getWalletDetail(widget.trxModel.userId, widget.trxModel.walletFromId);
        getWalletTo(widget.trxModel.userId, widget.trxModel.walletToId);
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var currency =
        NumberFormat.currency(locale: "id_ID", symbol: "Rp ", decimalDigits: 0);
    return Padding(
      padding: const EdgeInsets.only(top: 16.0, bottom: 16.0),
      child: Container(
        child: Row(
          children: [
            widget.trxModel.isTransfer == 1
                ? CircleAvatar(
                    backgroundColor: Colors.grey[300],
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      
                      children: [
                        Icon(
                          iconCode.getIcon(walletData.icon, walletData.iconFam),
                          color: Theme.of(context).colorScheme.secondary,
                          size: 15,
                        ),
                        Icon(
                          FontAwesomeIcons.exchangeAlt,
                          color: Theme.of(context).colorScheme.secondary,
                          size: 10,
                        ),
                        Icon(
                          iconCode.getIcon(walletDataToTransfer.icon,
                              walletDataToTransfer.iconFam),
                          color: Theme.of(context).colorScheme.secondary,
                          size: 15,
                        ),
                      ],
                    ),
                    radius: 25,
                  )
                : CircleAvatar(
                    backgroundColor: Colors.grey[300],
                    child: Icon(
                      iconCode.getIcon(walletData.icon, walletData.iconFam),
                      color: Theme.of(context).colorScheme.secondary,
                    ),
                    radius: 25,
                  ),
            Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 10),
                    child: Text(
                      walletData.name,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text(
                    widget.trxModel.description,
                    // style: TextStyle(fontSize: 12),
                  )
                ],
              ),
            ),
            Spacer(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Text(
                  DateFormat.yMMMMd('en_US').format(widget.trxModel.trxDate),
                  style: TextStyle(fontSize: 12, color: Colors.grey.shade700),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 14.0),
                  child: Text(
                    (widget.trxModel.isIncome == 1)
                        ? "+${currency.format(widget.trxModel.amount)}"
                        : (widget.trxModel.isExpense == 1)
                            ? "-${currency.format(widget.trxModel.amount)}"
                            : "${currency.format(widget.trxModel.amount)}",
                    style: TextStyle(
                      color: (widget.trxModel.isIncome == 1)
                          ? Theme.of(context).colorScheme.secondary
                          : (widget.trxModel.isExpense == 1)
                              ? Theme.of(context).colorScheme.error
                              : Theme.of(context).colorScheme.primary,
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
