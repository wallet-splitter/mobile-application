import 'dart:async';
import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import '/Utility/base_url.dart';
import '/Model/user_model.dart';
import '/Utility/googleSignIn.dart';
import 'package:http/http.dart' as http;

class Profile extends StatefulWidget {
  const Profile({Key? key, required this.userData}) : super(key: key);

  final UserModel userData;

  @override
  _Profile createState() => _Profile();
}

// void main() {
//   const str = "mariaevangelicaeleanror@gmail.com";
//   const start = "maria";
//   const end = "....";

//   final startIndex = str.indexOf(start);
//   final endIndex = str.indexOf(end, startIndex + start.length);

//   print(str.substring(startIndex + start.length, endIndex)); // brown fox jumps
// }

class _Profile extends State<Profile> {
  final userFirebase = FirebaseAuth.instance.currentUser;

  ValueNotifier<UserModel> user =
      ValueNotifier<UserModel>(UserModel('', '', ''));
  ValueNotifier<double> balance = ValueNotifier<double>(0);

  TextEditingController _userNameController = new TextEditingController();
  TextEditingController _userEmailController = new TextEditingController();

  Future<void> getUser(String? email) async {
    Map<String, String> requestHeaders = {
      'email': email!,
    };

    final response =
        await http.post(Uri.parse(BaseURL.getUser), headers: requestHeaders);

    print(response.body);
    final data = jsonDecode(response.body);

    user.value = UserModel(
        data['output']['id'], data['output']['name'], data['output']['email']);
  }

  Future<void> getBalance(String userId) async {
    Map<String, String> requestHeaders = {
      'user-id': userId,
    };

    final response =
        await http.get(Uri.parse(BaseURL.getBalance), headers: requestHeaders);

    print(response.body);
    final data = jsonDecode(response.body);

    balance.value = double.parse(data['output']);
  }

  Future<int> editUser(String id, String userName) async {
    print('$id $userName');
    Map<String, String> requestHeaders = {
      'id': id,
    };

    final response = await http.post(
      Uri.parse(BaseURL.editUser),
      headers: requestHeaders,
      body: jsonEncode(<String, dynamic>{'name': userName}),
    );

    print(response.body);
    final data = jsonDecode(response.body);

    return data['error_schema']['error_code'];
  }

  @override
  void initState() {
    getUser(userFirebase!.email);
    getBalance(widget.userData.id);
    _userEmailController.text = userFirebase!.email!;
    _userNameController.text = widget.userData.name;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var currency =
        NumberFormat.currency(locale: "id_ID", symbol: "Rp ", decimalDigits: 0);
    //karena dia page makanya pake scaffold
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white10,
        elevation: 0,
        iconTheme: IconThemeData(
          color: Theme.of(context).colorScheme.primary,
        ),
        actions: [
          ValueListenableBuilder(
              valueListenable: user,
              builder: (BuildContext context, UserModel value, Widget? child) {
                return TextButton(
                  child: Text('Save'),
                  onPressed: () async {
                    // return print('NAME >> ${_userNameController.text}');
                    int tryEdit =
                        await editUser(value.id, _userNameController.text)
                            .then((value) => value);
                    if (tryEdit == 1) {
                      print('Edit User Failed');
                    } else {
                      Navigator.pop(context);
                    }
                  },
                  style: TextButton.styleFrom(
                    primary: Theme.of(context).colorScheme.primary,
                    textStyle:
                        TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                );
              }),
        ],
      ),
      body: SafeArea(
        //listview >> scroll
        child: RefreshIndicator(
          onRefresh: () async {
            getUser(userFirebase!.email);
          },
          child: ListView(
            padding: EdgeInsets.all(20),
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 20),
                child: Align(
                  alignment: Alignment.center,
                  child: CircleAvatar(
                    backgroundImage:
                        NetworkImage(userFirebase!.photoURL.toString()),
                    backgroundColor: Colors.grey,
                    radius: 50,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.center,
                child: Padding(
                    padding: const EdgeInsets.only(left: 20, top: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment
                          .center, //Center Row contents horizontally,
                      children: [
                        Text(
                          'Balance : ',
                          style: TextStyle(fontSize: 22),
                        ),
                        ValueListenableBuilder(
                            valueListenable: balance,
                            builder: (context, value, Widget? child) {
                              return Text(currency.format(value),
                                  style: TextStyle(
                                    fontSize: 22,
                                  ));
                            }),
                      ],
                    )),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 20),
                child: ValueListenableBuilder(
                    valueListenable: user,
                    builder: (context, value, Widget? child) {
                      _userNameController.text = user.value.name;
                      return TextField(
                        style: TextStyle(
                          fontSize: 18,
                        ),
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          border: UnderlineInputBorder(),
                          labelText: 'Name',
                        ),
                        autofocus: false,
                        controller: _userNameController,
                      );
                    }),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 20),
                child: TextField(
                  style: TextStyle(
                    fontSize: 18,
                  ),
                  enabled: false,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    border: UnderlineInputBorder(),
                    labelText: 'Email',
                  ),
                  autofocus: false,
                  controller: _userEmailController,
                ),
              ),
              SizedBox(
                height: 65,
              ),
              Column(
                children: [
                  Container(
                    width: 250,
                    height: 65,
                    child: TextButton(
                      style: ButtonStyle(
                        foregroundColor: MaterialStateProperty.all<Color>(
                          Theme.of(context).colorScheme.error,
                        ),
                        backgroundColor: MaterialStateProperty.all<Color>(
                            Colors.transparent),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                    side: BorderSide(
                                        color:
                                            Theme.of(context).colorScheme.error,
                                        width: 3))),
                      ),
                      onPressed: () {
                        final provider = Provider.of<GoogleSignInProvider>(
                            context,
                            listen: false);
                        provider.logout();
                        Navigator.pop(context);
                      },
                      child: Text(
                        'Sign out',
                        style: TextStyle(
                            fontSize: 22, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
