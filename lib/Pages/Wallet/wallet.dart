import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_iconpicker/flutter_iconpicker.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '/Model/user_model.dart';
import '/Model/wallet_model.dart';
import '/Utility/base_url.dart';
import 'package:http/http.dart' as http;

import 'wallet_card.dart';

class Wallet extends StatefulWidget {
  const Wallet({Key? key}) : super(key: key);

  @override
  State<Wallet> createState() => _WalletState();
}

class _WalletState extends State<Wallet> {
  final user = FirebaseAuth.instance.currentUser;

  UserModel userData = UserModel('', '', '');
  List<Widget> walletList = <Widget>[];
  List<bool> _isFavorited = List.generate(10, (index) => false);

  ValueNotifier<IconData> _icon =
      ValueNotifier<IconData>(FontAwesomeIcons.dollarSign);

  Future<void> getWallets(String? email) async {
    Map<String, String> requestHeadersUser = {
       'email': email!,
     };

    final responseUser = await http.post(Uri.parse(BaseURL.getUser), headers: requestHeadersUser);
    final dataUser = jsonDecode(responseUser.body);
    
    setState(() {
      userData = UserModel(dataUser['output']['id'], dataUser['output']['name'], dataUser['output']['email']);
    });
    
    Map<String, String> requestHeaders = {
      'user-id': dataUser['output']['id'],
    };

    final response =
        await http.get(Uri.parse(BaseURL.getWallets), headers: requestHeaders);

    print(response.body);
    final data = jsonDecode(response.body);

    List wallets = data['output'];

    List<Widget> tempList = <Widget>[];

    for (int i = 0; i < wallets.length; i++) {
      WalletModel walletModel = WalletModel(
          wallets[i]['id'],
          wallets[i]['user_id'],
          wallets[i]['icon'],
          wallets[i]['icon_family'],
          wallets[i]['name'],
          wallets[i]['balance'],
          wallets[i]['is_main']);

      if (wallets[i]['is_favorite'] == '1') {
        _isFavorited[i] = true;
      }

      tempList.add(WalletCard(
        walletModel: walletModel,
        value: _isFavorited[i],
        onChanged: (bool newValue) async {
          setState(() {
            _isFavorited[i] = newValue;
          });
          print('Wallet ke-$i');
          print('Bool _isFavorited = ${_isFavorited[i]}');

          int tryUpdateFav = await updateFav(
                  wallets[i]['user_id'], wallets[i]['id'], _isFavorited[i])
              .then((value) => value);
          if (tryUpdateFav == 1) {
            final snackBar = SnackBar(
              content: const Text(
                'Failed to update favorite wallet!',
                textAlign: TextAlign.center,
              ),
            );
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          }
        },
      ));
    }

    setState(() {
      walletList = tempList;
    });
  }

  _pickIcon() async {
    IconData? icon = await FlutterIconPicker.showIconPicker(context,
        iconPackMode: IconPack.fontAwesomeIcons);

    if (icon != null) _icon.value = icon;
    //setState update perubahan tanpa ada yang harus di panggil sama kaya refresh
    setState(() {});

    debugPrint('Picked Icon:  ${_icon.value.codePoint.toInt()}');
  }

  Future<int> addWallet(String userId, int walletIcon, String? walletIconFam,
      String walletName) async {
    Map<String, String> requestHeaders = {
      'user-id': userId,
    };

    final response = await http.post(
      Uri.parse(BaseURL.addWallet),
      headers: requestHeaders,
      body: jsonEncode(<String, dynamic>{
        'wallet_icon': walletIcon,
        'wallet_icon_family': walletIconFam,
        'wallet_name': walletName
      }),
    );

    print(response.body);
    final data = jsonDecode(response.body);

    return data['error_schema']['error_code'];
  }

  Future<int> updateFav(String userId, String walletId, bool isFav) async {
    String fav = '0';

    if (isFav) fav = '1';

    print('$userId, $walletId, $fav');

    Map<String, String> requestHeaders = {
      'user-id': userId,
      'wallet-id': walletId,
      'is-favorite': fav,
    };

    final response = await http.post(Uri.parse(BaseURL.updateFavWallets),
        headers: requestHeaders);

    print(response.body);
    final data = jsonDecode(response.body);

    return data['error_schema']['error_code'];
  }

  @override
  void initState() {
    getWallets(user!.email);
    // _icon = FontAwesomeIcons.dollarSign;
    super.initState();
  }

  TextEditingController _walletNameController = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white10,
        elevation: 0,
        centerTitle: false,
        title: Padding(
          padding: const EdgeInsets.only(left: 5),
          child: Text(
            "Wallet",
            style: TextStyle(
              color: Colors.black,
              fontSize: 34,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: TextButton(
              child: Text('Add Wallet'),
              onPressed: walletList.length == 10
                  ? null
                  : () {
                      modalAddWallet(context);
                    },
              style: TextButton.styleFrom(
                primary: Theme.of(context).colorScheme.primary,
                textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
      body: SafeArea(
        child: Container(
          //listview >> scroll
          child: RefreshIndicator(
            onRefresh: () async {
              setState(() {
                getWallets(user!.email);
              });
            },
            child: ListView(
              // physics: ClampingScrollPhysics(),
              padding: EdgeInsets.only(bottom: 20),
              children: [
                SizedBox(
                  height: 25,
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      '${walletList.length}/10',
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                ),
                Column(
                  children: walletList,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future modalAddWallet(BuildContext context) {
    return showModalBottomSheet(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(top: Radius.circular(25.0))),
        backgroundColor: Colors.white,
        context: context,
        isScrollControlled: true,
        builder: (context) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Align(
                      alignment: Alignment.center,
                      child: Container(
                        height: 5,
                        width: 85,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.black,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        TextButton(
                          child: Text('Cancel'),
                          onPressed: () {
                            Navigator.pop(context);
                            getWallets(user!.email);
                          },
                          style: TextButton.styleFrom(
                            primary: Theme.of(context).colorScheme.primary,
                            alignment: Alignment.centerLeft,
                            textStyle: TextStyle(
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        TextButton(
                          child: Text('Save'),
                          onPressed: _walletNameController.text == ''
                              ? null
                              : () async {
                                  int tryAdd = await addWallet(
                                          userData.id,
                                          _icon.value.codePoint,
                                          _icon.value.fontFamily,
                                          _walletNameController.text)
                                      .then((value) => value);
                                  if (tryAdd == 1) {
                                    print('Edit Wallet Failed');
                                  } else {
                                    //pop -> mundur 1 module
                                    Navigator.pop(context);
                                    //balik jadi default sesudah pop
                                    _icon.value = FontAwesomeIcons.dollarSign;
                                    _walletNameController.text = "";
                                    getWallets(user!.email);
                                  }
                                },
                          style: TextButton.styleFrom(
                            primary: Theme.of(context).colorScheme.primary,
                            textStyle: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 35, bottom: 35),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        "Add New Wallet",
                        style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 20, bottom: 20),
                    child: Align(
                      alignment: Alignment.center,
                      child: Stack(
                        children: [
                          CircleAvatar(
                            backgroundColor: Colors.grey[300],
                            radius: 50,
                            child: AnimatedSwitcher(
                              duration: Duration(milliseconds: 300),
                              child: ValueListenableBuilder<IconData>(
                                  valueListenable: _icon,
                                  builder: (context, value, Widget? child) {
                                    return Icon(
                                      value,
                                      color: Theme.of(context)
                                          .colorScheme
                                          .secondary,
                                      size: 50,
                                    );
                                  }),
                            ),
                          ),
                          Positioned.fill(
                            child: Align(
                              alignment: Alignment.bottomRight,
                              child: Stack(
                                children: [
                                  CircleAvatar(
                                    backgroundColor:
                                        Theme.of(context).colorScheme.primary,
                                    radius: 12.0,
                                    child: Icon(
                                      FontAwesomeIcons.pencilAlt,
                                      size: 10,
                                      color: Colors.white,
                                    ),
                                  ),
                                  Positioned.fill(
                                    child: Material(
                                      color: Colors.transparent,
                                      //InkWell -> hyperlink / panggil pick icon
                                      child: InkWell(
                                        customBorder: CircleBorder(),
                                        onTap: () => _pickIcon(),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 35,
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom),
                    child: TextField(
                      maxLength: 20,
                      style: TextStyle(
                        fontSize: 22,
                      ),
                      controller: _walletNameController,
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        border: UnderlineInputBorder(),
                        labelText: 'Wallet Name',
                      ),
                      // controller: _newMediaLinkAddressController,
                    ),
                  ),
                  SizedBox(height: 35),
                ],
              ),
            ));
  }
}
