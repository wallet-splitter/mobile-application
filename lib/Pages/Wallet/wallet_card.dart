import 'package:auto_size_text/auto_size_text.dart';
import 'package:favorite_button/favorite_button.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:wallet_splitter/Model/wallet_splitplan_model.dart';
import '/Model/wallet_model.dart';
import '/Utility/icon_code.dart' as iconCode;

import 'wallet_detail.dart';

class WalletCard extends StatefulWidget {
  const WalletCard({Key? key, required this.walletModel, required this.value,
    required this.onChanged,}) : super(key: key);

  final WalletModel walletModel;
  final bool value;
  final Function onChanged;

  @override
  State<WalletCard> createState() => _WalletCardState();
}

class _WalletCardState extends State<WalletCard> {
  var currency =
      NumberFormat.currency(locale: "id_ID", symbol: "Rp ", decimalDigits: 0);
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 20, right: 20, top: 20),
      // height: 80,
      width: double.maxFinite,
      child: Card(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: Theme.of(context).colorScheme.primary,
        elevation: 5,
        child: InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => WaletDetail(
                      userId: widget.walletModel.userId,
                      walletId: widget.walletModel.id),
                ));
          },
          child: Padding(
            padding: EdgeInsets.all(20),
            child: Stack(
              children: [
                Column(
                  children: [
                    Row(
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.white,
                          child: Icon(
                            iconCode.getIcon(widget.walletModel.icon,
                                widget.walletModel.iconFam),
                            color: Theme.of(context).colorScheme.secondary,
                          ),
                          radius: 23,
                        ),
                        Expanded(
                          child: Padding(
                            padding: EdgeInsets.only(left: 20),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                AutoSizeText(
                                  widget.walletModel.name,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                      fontSize: 22),
                                ),
                                Text(currency.format(widget.walletModel.balance),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18,
                                        fontWeight: FontWeight.bold)),
                              ],
                            ),
                          ),
                        ),
                        StarButton(
                          iconSize: 40,
                          isStarred: widget.value,
                          valueChanged: (bool? newValue) {
                              widget.onChanged(newValue);
                            },
                        ),
                      ],
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class WalletCheckbox extends StatelessWidget {
  const WalletCheckbox({
    Key? key,
    required this.wallet,
    required this.value,
    required this.onChanged,
  }) : super(key: key);

  final WalletSplitPlanModel wallet;
  final bool value;
  final Function onChanged;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 20),
      width: double.maxFinite,
      child: Stack(
        children: [
          Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            color: Theme.of(context).colorScheme.primary,
            elevation: 5,
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                children: [
                  Row(
                    children: [
                      CircleAvatar(
                        backgroundColor: Colors.white,
                        child: Icon(
                          iconCode.getIcon(wallet.icon, wallet.iconFam),
                          color: Theme.of(context).colorScheme.secondary,
                        ),
                        radius: 23,
                      ),
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 20),
                          child: AutoSizeText(
                            wallet.name,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                              fontSize: 22,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 18,
                        height: 18,
                        child: Container(
                          color: Colors.white,
                          child: Checkbox(
                            value: value,
                            onChanged: (bool? newValue) {
                              onChanged(newValue);
                            },
                            activeColor:
                                Theme.of(context).colorScheme.secondary,
                            side: BorderSide(
                              color: Colors.transparent,
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          Positioned.fill(
            child: Padding(
              padding: const EdgeInsets.all(4),
              child: new Material(
                color: Colors.transparent,
                child: new InkWell(
                  customBorder: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  onTap: () {
                    onChanged(!value);
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class WalletPrecentage extends StatefulWidget {
  const WalletPrecentage({
    Key? key,
    required this.wallet,
    required this.onChanged,
  }) : super(key: key);

  final WalletSplitPlanModel wallet;
  final Function onChanged;

  @override
  State<WalletPrecentage> createState() => _WalletPrecentageState();
}

class _WalletPrecentageState extends State<WalletPrecentage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 20),
      width: double.maxFinite,
      child: Stack(
        children: [
          Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            color: Theme.of(context).colorScheme.primary,
            elevation: 5,
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                children: [
                  Row(
                    children: [
                      CircleAvatar(
                        backgroundColor: Colors.white,
                        child: Icon(
                          iconCode.getIcon(
                              widget.wallet.icon, widget.wallet.iconFam),
                          color: Theme.of(context).colorScheme.secondary,
                        ),
                        radius: 23,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: AutoSizeText(
                                widget.wallet.name,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontSize: 22,
                                ),
                              ),
                            ),
                            Slider(
                              activeColor: Colors.white,
                              divisions: 20,
                              value: widget.wallet.percentage,
                              min: 0,
                              max: 100,
                              label:
                                  widget.wallet.percentage.round().toString() +
                                      '%',
                              onChanged: (value) {
                                widget.onChanged(value);
                              },
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Text(
                          widget.wallet.percentage.round().toString() + '%',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            fontSize: 18,
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class WalletPercenatgeView extends StatelessWidget {
  const WalletPercenatgeView({Key? key, required this.wallet})
      : super(key: key);

  final WalletSplitPlanModel wallet;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 20),
      width: double.maxFinite,
      child: Stack(
        children: [
          Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            color: Theme.of(context).colorScheme.primary,
            elevation: 5,
            child: Padding(
              padding: EdgeInsets.all(20),
              child: Column(
                children: [
                  Row(
                    children: [
                      CircleAvatar(
                        backgroundColor: Colors.white,
                        child: Icon(
                          iconCode.getIcon(wallet.icon, wallet.iconFam),
                          color: Theme.of(context).colorScheme.secondary,
                        ),
                        radius: 23,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(left: 20),
                              child: AutoSizeText(
                                wallet.name,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white,
                                  fontSize: 22,
                                ),
                              ),
                            ),
                            Slider(
                              activeColor: Colors.white,
                              divisions: 20,
                              value: wallet.percentage,
                              min: 0,
                              max: 100,
                              label: wallet.percentage.round().toString() + '%',
                              onChanged: null,
                            ),
                          ],
                        ),
                      ),
                      Container(
                        child: Text(
                          wallet.percentage.round().toString() + '%',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                            fontSize: 18,
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
