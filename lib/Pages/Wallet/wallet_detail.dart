import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:wallet_splitter/Model/transaction_model.dart';
import '/Model/wallet_model.dart';
import '/Pages/Wallet/wallet_edit.dart';
import '/Utility/base_url.dart';
import 'package:http/http.dart' as http;
import '/Utility/icon_code.dart' as iconCode;

class WaletDetail extends StatefulWidget {
  const WaletDetail({Key? key, required this.userId, required this.walletId})
      : super(key: key);

  final String userId;
  final String walletId;

  @override
  _WaletDetailState createState() => _WaletDetailState();
}

class _WaletDetailState extends State<WaletDetail> {
  final user = FirebaseAuth.instance.currentUser;
  WalletModel walletData = WalletModel('', '', '0', '', '', '0', '0');

  List<Widget> historyList = <Widget>[];

  Future<void> getWalletDetail(String userId, String walletId) async {
    Map<String, String> requestHeaders = {
      'user-id': userId,
      'wallet-id': walletId
    };

    final response = await http.get(Uri.parse(BaseURL.getWalletSingle),
        headers: requestHeaders);

    print(response.body);
    final data = jsonDecode(response.body);
    late Map wallets;
    if (data['error_schema']['error_code'] == 0) {
      wallets = data['output'];
      setState(() {
        walletData = WalletModel(wallets['id'], wallets['user_id'],
            wallets['icon'], wallets['icon_family'], wallets['name'], wallets['balance'], wallets['is_main']);
      });
    }

    final responseHistory = await http.post(Uri.parse(BaseURL.getWalletHistory),
        headers: requestHeaders);

    print(responseHistory.body);
    final dataHistory = jsonDecode(responseHistory.body);
    List<Widget> tempList = <Widget>[];

    if (dataHistory['error_schema']['error_code'] == 0) {
      List history = dataHistory['output'];

      for (int i = 0; i < history.length; i++) {
        TransactionModel trxModel = TransactionModel(
            history[i]['id'],
            history[i]['user_id'],
            history[i]['wallet_from_id'],
            history[i]['wallet_to_id'],
            history[i]['amount'],
            history[i]['description'],
            history[i]['date'],
            history[i]['is_income'],
            history[i]['is_expense'],
            history[i]['is_transfer']);
        tempList.add(WalletDetailHistory(
          trxModel: trxModel,
          walletIcon: int.parse(wallets['icon']),
          walletIconFam: wallets['icon_family']
        ));
      }

      setState(() {
        historyList = tempList;
      });
    } else if (dataHistory['error_schema']['error_code'] == 1){
      Widget noHistoryWidget = Align(
        alignment: Alignment.center,
        child: Text('No History', style: TextStyle(fontSize: 18),),
      );

      tempList.add(noHistoryWidget);
      setState(() {
        historyList = tempList;
      });
    }
  }

  @override
  void initState() {
    getWalletDetail(widget.userId, widget.walletId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var currency =
        NumberFormat.currency(locale: "id_ID", symbol: "Rp ", decimalDigits: 0);
    //karena dia page makanya pake scaffold
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        actions: [
          TextButton(
            child: Text(walletData.isMain==1 ? '':'Edit'),
            onPressed: walletData.isMain==1 ? null:
            () {
              Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (_) => WalletEdit(
                          walletData: walletData,
                        ),
                      ))
                  .then((_) => getWalletDetail(widget.userId, widget.walletId));
            },
            style: TextButton.styleFrom(
              primary: Colors.white,
              textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
      body: SafeArea(
        //listview >> scroll
        child: RefreshIndicator(
          onRefresh: () async {
            setState(() {
              getWalletDetail(widget.userId, widget.walletId);
            });
          },
          child: ListView(
            physics: ClampingScrollPhysics(),
            children: [
              Container(
                    color: Theme.of(context).colorScheme.primary,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(top: 20, bottom: 20),
                          child: Align(
                            alignment: Alignment.center,
                            child: CircleAvatar(
                              backgroundColor: Colors.white,
                              radius: 50,
                              child: Icon(
                                iconCode.getIcon(walletData.icon, walletData.iconFam),
                                color: Theme.of(context).colorScheme.secondary,
                                size: 50,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 20),
                          child: Text(
                            walletData.name,
                            style: TextStyle(
                              fontSize: 22,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 20),
                          child: Text(
                            currency.format(walletData.balance),
                            style: TextStyle(
                              fontSize: 24,
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                      ],
                    ),
                  ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 20, top: 10, bottom: 60, right: 20),
                child: Stack(children: [
                  Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Recent History",
                            style: TextStyle(
                              fontSize: 22,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          // TextButton.icon(
                          //   onPressed: () {
                          //     Navigator.pop(context);
                          //   },
                          //   label: Text(
                          //     '',
                          //     style: TextStyle(
                          //         fontSize: 22, fontWeight: FontWeight.bold),
                          //   ),
                          //   icon: FaIcon(
                          //     FontAwesomeIcons.history,
                          //     color: Theme.of(context).colorScheme.primary,
                          //   ),
                          //   style: TextButton.styleFrom(
                          //     primary: Theme.of(context).colorScheme.primary,
                          //     textStyle: TextStyle(
                          //         fontSize: 18, fontWeight: FontWeight.bold),
                          //   ),
                          // ),
                        ],
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Column(
                        children: historyList,
                      ),
                    ],
                  )
                ]),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class WalletDetailHistory extends StatefulWidget {
  const WalletDetailHistory({Key? key, required this.trxModel, required this.walletIcon, required this.walletIconFam})
      : super(key: key);

  final TransactionModel trxModel;
  final int walletIcon;
  final String walletIconFam;

  @override
  State<WalletDetailHistory> createState() => _WalletDetailHistoryState();
}

class _WalletDetailHistoryState extends State<WalletDetailHistory> {
  
  WalletModel walletDataFromTransfer = WalletModel('', '', '0', '', '', '0', '0');
  WalletModel walletDataToTransfer = WalletModel('', '', '0', '', '', '0', '0');

  Future<void> getWalletTo(String userId, String walletId) async {
    Map<String, String> requestHeaders = {
      'user-id': userId,
      'wallet-id': walletId
    };

    final response = await http.get(Uri.parse(BaseURL.getWalletSingle),
        headers: requestHeaders);

    print(response.body);
    final data = jsonDecode(response.body);

    if (data['error_schema']['error_code'] == 0) {
      Map wallets = data['output'];

      setState(() {
        walletDataToTransfer = WalletModel(
            wallets['id'],
            wallets['user_id'],
            wallets['icon'],
            wallets['icon_family'],
            wallets['name'],
            wallets['balance'],
            wallets['is_main']);
      });
    }
  }

  Future<void> getWalletFrom(String userId, String walletId) async {
    Map<String, String> requestHeaders = {
      'user-id': userId,
      'wallet-id': walletId
    };

    final response = await http.get(Uri.parse(BaseURL.getWalletSingle),
        headers: requestHeaders);

    print(response.body);
    final data = jsonDecode(response.body);

    if (data['error_schema']['error_code'] == 0) {
      Map wallets = data['output'];

      setState(() {
        walletDataFromTransfer = WalletModel(
            wallets['id'],
            wallets['user_id'],
            wallets['icon'],
            wallets['icon_family'],
            wallets['name'],
            wallets['balance'],
            wallets['is_main']);
      });
    }
  }
  
  var currency =
      NumberFormat.currency(locale: "id_ID", symbol: "Rp ", decimalDigits: 0);
  
  @override
  void initState() {
    if (widget.trxModel.isTransfer==1){
      getWalletFrom(widget.trxModel.userId, widget.trxModel.walletFromId);
      getWalletTo(widget.trxModel.userId, widget.trxModel.walletToId);
    }
    super.initState();
  }
  
  @override
  Widget build(BuildContext context) {
    return Container(
        child: SafeArea(
            child: RefreshIndicator(
                onRefresh: () async {
                  return print("Refresh");
                },
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 10),
                  child: Column(children: [
                    Row(children: [
                      widget.trxModel.isTransfer == 1?
                      CircleAvatar(
                        backgroundColor: Colors.grey[300],
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          
                          children: [
                            Icon(
                              iconCode.getIcon(walletDataFromTransfer.icon,
                                  walletDataFromTransfer.iconFam),
                              color: Theme.of(context).colorScheme.secondary,
                              size: 15,
                            ),
                            Icon(
                              FontAwesomeIcons.exchangeAlt,
                              color: Theme.of(context).colorScheme.secondary,
                              size: 10,
                            ),
                            Icon(
                              iconCode.getIcon(walletDataToTransfer.icon,
                                  walletDataToTransfer.iconFam),
                              color: Theme.of(context).colorScheme.secondary,
                              size: 15,
                            ),
                          ],
                        ),
                        radius: 25,
                      ):
                      CircleAvatar(
                        backgroundColor: Colors.grey[300],
                        child: Icon(
                          iconCode.getIcon(widget.walletIcon, widget.walletIconFam),
                          color: Theme.of(context).colorScheme.secondary,
                        ),
                        radius: 23,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.only(bottom: 10),
                              child: Text(
                                widget.trxModel.description,
                                style: TextStyle(fontSize: 12),
                              ),
                            ),
                            Text(
                              (widget.trxModel.isIncome == 1)
                                  ? "+${currency.format(widget.trxModel.amount)}"
                                  : (widget.trxModel.isExpense == 1)
                                      ? "-${currency.format(widget.trxModel.amount)}"
                                      : "${currency.format(widget.trxModel.amount)}",
                              style: TextStyle(
                                color: (widget.trxModel.isIncome == 1)
                                    ? Theme.of(context).colorScheme.secondary
                                    : (widget.trxModel.isExpense == 1)
                                        ? Theme.of(context).colorScheme.error
                                        : Theme.of(context).colorScheme.primary,
                                fontSize: 12,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Spacer(),
                      Text(
                        DateFormat.yMMMMd('en_US').format(widget.trxModel.trxDate),
                        style: TextStyle(fontSize: 12),
                      ),
                    ])
                  ]),
                ))));
  }
}
