import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_iconpicker/flutter_iconpicker.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '/Utility/base_url.dart';
import '/Model/wallet_model.dart';
import '/Utility/icon_code.dart' as iconCode;
import 'package:http/http.dart' as http;

class WalletEdit extends StatefulWidget {
  const WalletEdit({Key? key, required this.walletData}) : super(key: key);

  final WalletModel walletData;

  @override
  _WalletEdit createState() => _WalletEdit();
}

class _WalletEdit extends State<WalletEdit> {
  final user = FirebaseAuth.instance.currentUser;

  TextEditingController _walletNameController = new TextEditingController();
  late IconData _icon;

  Future<int> deleteWallet(String userId, String walletId) async {
    Map<String, String> requestHeaders = {
      'user-id': userId,
      'wallet-id': walletId
    };

    final response =
        await http.post(Uri.parse(BaseURL.deleteWallet), headers: requestHeaders);

    print(response.body);
    final data = jsonDecode(response.body);

    return data['error_schema']['error_code'];
  }

  Future<int> editWallet(
      String userId, String walletId, int walletIcon, String? walletIconFam, String walletName) async {
    Map<String, String> requestHeaders = {
      'user-id': userId,
    };

    final response = await http.post(
      Uri.parse(BaseURL.editWallet),
      headers: requestHeaders,
      body: jsonEncode(<String, dynamic>{
        'wallet_id': walletId,
        'wallet_icon': walletIcon,
        'wallet_icon_family': walletIconFam,
        'wallet_name': walletName
      }),
    );

    print(response.body);
    final data = jsonDecode(response.body);

    return data['error_schema']['error_code'];
  }

  @override
  void initState() {
    _icon = iconCode.getIcon(widget.walletData.icon, widget.walletData.iconFam);
    _walletNameController.text = widget.walletData.name;
    super.initState();
  }

  void _deleteConfirmationDialog() {
    AlertDialog alertDialog = AlertDialog(
      title: Text('Delete Wallet'),
      content:
          Text('Are you sure to delete this ${_walletNameController.text}?'),
      actions: <Widget>[
        TextButton(
          onPressed: () => Navigator.pop(context, 'No'),
          child: Text('No'),
        ),
        TextButton(
          onPressed: () async {
            Navigator.pop(context, 'Yes');
            int tryDelete = await deleteWallet(widget.walletData.userId, widget.walletData.id).then((value) => value);
            if (tryDelete == 1) {
                print('Edit Wallet Failed');
              } else {
                Navigator.pop(context);
                Navigator.pop(context);
              }
          },
          child: Text('Yes'),
        ),
      ],
    );
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return alertDialog;
        });
  }

  _pickIcon() async {
    IconData? icon = await FlutterIconPicker.showIconPicker(context,
        iconPackMode: IconPack.fontAwesomeIcons);

    if (icon != null) _icon = icon;

    setState(() {});

    debugPrint('Picked Icon:  ${_icon.codePoint.toInt()}');
  }

  @override
  Widget build(BuildContext context) {
    //karena dia page makanya pake scaffold
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white10,
        elevation: 0,
        iconTheme: IconThemeData(
          color: Theme.of(context).colorScheme.primary,
        ),
        actions: [
          TextButton(
            child: Text('Save'),
            onPressed: () async {
              if(_walletNameController.text==''){
                final snackBar = SnackBar(
                  content: const Text(
                    'Missing field!',
                    textAlign: TextAlign.center,
                  ),
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
              } else {
                int tryEdit = await editWallet(
                        widget.walletData.userId,
                        widget.walletData.id,
                        _icon.codePoint,
                        _icon.fontFamily,
                        _walletNameController.text)
                    .then((value) => value);
                if (tryEdit == 1) {
                  print('Edit Wallet Failed');
                } else {
                  Navigator.pop(context);
                }
              }
            },
            style: TextButton.styleFrom(
              primary: Theme.of(context).colorScheme.primary,
              textStyle: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
      body: SafeArea(
        //listview >> scroll
        child: RefreshIndicator(
          onRefresh: () async {
            return print("Refresh");
          },
          child: ListView(
            children: [
              Column(
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 20, bottom: 20),
                    child: Stack(
                      children: [
                        CircleAvatar(
                          backgroundColor: Colors.grey[300],
                          radius: 50,
                          child: AnimatedSwitcher(
                            duration: Duration(milliseconds: 300),
                            child: Icon(
                              _icon,
                              color: Theme.of(context).colorScheme.secondary,
                              size: 50,
                            ),
                          ),
                        ),
                        Positioned.fill(
                          child: Align(
                            alignment: Alignment.bottomRight,
                            child: Stack(
                              children: [
                                CircleAvatar(
                                  backgroundColor:
                                      Theme.of(context).colorScheme.primary,
                                  radius: 12.0,
                                  child: Icon(
                                    FontAwesomeIcons.pencilAlt,
                                    size: 10,
                                    color: Colors.white,
                                  ),
                                ),
                                Positioned.fill(
                                  child: Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      customBorder: CircleBorder(),
                                      onTap: () => _pickIcon(),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                    child: TextField(
                      controller: _walletNameController,
                      maxLength: 20,
                      style: TextStyle(fontSize: 22),
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                        border: UnderlineInputBorder(),
                        labelText: 'Wallet Name',
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 65,
                  ),

                  TextButton(
                    style: ButtonStyle(
                      foregroundColor: MaterialStateProperty.all<Color>(
                        Colors.white,
                      ),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Theme.of(context).colorScheme.error),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              side: BorderSide(color: Colors.red))),
                      padding: MaterialStateProperty.all<EdgeInsets>(
                        EdgeInsets.only(
                            top: 13, bottom: 13, right: 25, left: 25),
                      ),
                    ),
                    onPressed: () {
                      _deleteConfirmationDialog();
                    },
                    child: Text(
                      'Delete Wallet',
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
