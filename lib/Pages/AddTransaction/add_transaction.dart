import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:wallet_splitter/Utility/base_url.dart';
import '/Model/user_model.dart';
import 'expense_transaction.dart';
import 'income_transaction.dart';
import 'transfer_transaction.dart';
import 'package:http/http.dart' as http;

class AddTransaction extends StatefulWidget {
  AddTransaction({Key? key}) : super(key: key);

  @override
  _AddTransactionState createState() => _AddTransactionState();
}

class _AddTransactionState extends State<AddTransaction> {
  final user = FirebaseAuth.instance.currentUser;
  
  Widget buildSegment(String text, Color color, bool isActive) {
    return Container(
      child: Text(text,
          style: TextStyle(
              fontSize: 12,
              color: isActive == true ? Colors.white : Colors.black)),
    );
  }

  UserModel userData = UserModel('', '', '');
  int currentTab = 0;
  // int groupValue = 0;
  final PageStorageBucket bucket = PageStorageBucket();
  final List<bool> segmentedCategory = [true, false, false];
  Widget currentPage = Center(child: CircularProgressIndicator());
  Color selectedColor = Colors.black;

  Future<void> getUser(String? email) async {
    Map<String, String> requestHeaders = {
       'email': email!,
     };

    final response = await http.post(Uri.parse(BaseURL.getUser), headers: requestHeaders);

    print(response.body);
    final data = jsonDecode(response.body);

    setState(() {
      userData = UserModel(data['output']['id'], data['output']['name'], data['output']['email']);
      currentPage = IncomeTransaction(userData: userData,);
    });
  }

  @override
  void initState() {
    super.initState();
    getUser(user!.email);
    // currentPage = IncomeTransaction(userData: userData,);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'Add Transaction',
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white10,
          elevation: 0,
          // Back Button
          iconTheme: IconThemeData(
            color: Theme.of(context).colorScheme.primary,
          ),
          
        ),
        body: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(children: [
                Row(
                  children: [
                    Expanded(
                      // Segmented Control
                      child: CupertinoSlidingSegmentedControl(
                          thumbColor: currentTab == 0
                              ? Theme.of(context).colorScheme.secondary
                              : currentTab == 1
                                  ? Theme.of(context).colorScheme.error
                                  : currentTab == 2
                                      ? Theme.of(context).colorScheme.primary
                                      : Color.fromRGBO(222, 222, 225, 1),
                          groupValue: currentTab,
                          children: {
                            0: buildSegment(
                                "Income", selectedColor, segmentedCategory[0]),
                            1: buildSegment(
                                "Expense", selectedColor, segmentedCategory[1]),
                            2: buildSegment(
                                "Transfer", selectedColor, segmentedCategory[2])
                          },
                          onValueChanged: (value) {
                            setState(() {
                              if (value == 0) {
                                currentPage = IncomeTransaction(userData: userData,);
                                currentTab = 0;
                                segmentedCategory[currentTab] = true;
                                segmentedCategory[1] = false;
                                segmentedCategory[2] = false;
                              } else if (value == 1) {
                                currentPage = ExpenseTransaction(userData: userData,);
                                currentTab = 1;
                                segmentedCategory[currentTab] = true;
                                segmentedCategory[0] = false;
                                segmentedCategory[2] = false;
                              } else {
                                currentPage = TransferTransaction(userData: userData,);
                                currentTab = 2;
                                segmentedCategory[currentTab] = true;
                                segmentedCategory[0] = false;
                                segmentedCategory[1] = false;
                              }
                            });
                          }),
                    ),
                  ],
                ),
                PageStorage(
                  child: currentPage,
                  bucket: bucket,
                ),
                // IncomeTransaction()
                // ExpenseTransaction()
                // TransferTransaction()
              ]),
            ),
          ],
        ),
      ),
    );
  }
}
