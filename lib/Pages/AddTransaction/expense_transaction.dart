import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:wallet_splitter/Model/user_model.dart';
import 'package:wallet_splitter/Model/wallet_model.dart';
import 'package:wallet_splitter/Utility/base_url.dart';
import 'package:http/http.dart' as http;

class ExpenseTransaction extends StatefulWidget {
  const ExpenseTransaction({Key? key, required this.userData}) : super(key: key);

  final UserModel userData;

  @override
  _ExpenseTransactionState createState() => _ExpenseTransactionState();
}

class _ExpenseTransactionState extends State<ExpenseTransaction> {
  List<WalletModel> walletData = [];
  WalletModel dropdownValue = WalletModel('','','0','','','0','0');
  DateTime selectedDate = DateTime.now();
  TextEditingController dateinput = TextEditingController();
  String sendDate = "";
  TextEditingController amountInput = TextEditingController();
  TextEditingController descriptionInput = TextEditingController();

  String _formatNumber(String s) =>
      NumberFormat.decimalPattern('id').format(int.parse(s));

  Future<int> _addExpense(String userId, WalletModel walletFrom, double amount,
      String description) async {
    Map<String, String> requestHeaders = {
      'user-id': userId,
    };

    final response = await http.post(
      Uri.parse(BaseURL.addTrx),
      headers: requestHeaders,
      body: jsonEncode(<String, dynamic>{
        'is_income': 0,
        'is_expense': 1,
        'is_transfer': 0,
        'wallet_from_id': walletFrom.id,
        'amount': amount,
        'description': description,
        'trx_date': sendDate
      }),
    );

    print(response.body);
    final data = jsonDecode(response.body);

    return data['error_schema']['error_code'];
  }

  Future<void> getWallets() async {
    
    Map<String, String> requestHeaders = {
      'user-id': widget.userData.id,
    };

    final response =
        await http.get(Uri.parse(BaseURL.getWallets), headers: requestHeaders);

    print(response.body);
    final data = jsonDecode(response.body);

    List wallets = data['output'];

    List<WalletModel> tempList = <WalletModel>[];

    for (int i = 0; i < wallets.length; i++) {
      WalletModel walletModel = WalletModel(
          wallets[i]['id'],
          wallets[i]['user_id'],
          wallets[i]['icon'],
          wallets[i]['icon_family'],
          wallets[i]['name'],
          wallets[i]['balance'],
          wallets[i]['is_main']);
      tempList.add(walletModel);
    }

    setState(() {
      dropdownValue = tempList[0];
      walletData = tempList;
    });
  }
  
  @override
  void initState() {
    getWallets();
    dateinput.text = "";
    super.initState();
    amountInput.text = '0';
    // dropdownValue = widget.walletData[0];
    dateinput.text = DateFormat.yMMMMd('en_US').format(DateTime.now());
    sendDate = DateFormat('yyyy-MM-dd').format(DateTime.now());
  }

  Widget build(BuildContext context) {
    return Column(children: [
      Padding(
          padding: const EdgeInsets.only(top: 40),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              'Amount',
              style: TextStyle(fontSize: 22),
            ),
          )),
      TextFormField(
        controller: amountInput,
        maxLength: 15,
        keyboardType: TextInputType.number,
        style: TextStyle(fontSize: 18),
        decoration: InputDecoration(
            hintText: '0',
            prefixText: 'Rp ',
            label: Text('Rp '),
            alignLabelWithHint: true,
            floatingLabelBehavior: FloatingLabelBehavior.never,
            counterText: ''),
        onChanged: (amt){
          if(amt=='')
            amt='0';
          amt = '${_formatNumber(amt.replaceAll('.', ''))}';
          amountInput.value = TextEditingValue(
            text: amt,
            selection: TextSelection.collapsed(offset: amt.length),
          );
        },
      ),

      // From
      Padding(
        padding: const EdgeInsets.only(top: 22.0),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            'From',
            style: TextStyle(fontSize: 22),
          ),
        ),
      ),

      Container(
        decoration: BoxDecoration(border: Border.symmetric()),
        child: DropdownButton<WalletModel>(
          value: dropdownValue,
          icon: const Icon(Icons.arrow_downward),
          iconSize: 24,
          elevation: 16,
          underline: Container(height: 2),
          isExpanded: true,
          onChanged: (WalletModel? newValue) {
            setState(() {
              dropdownValue = newValue!;
            });
          },
          items: walletData
              .map((value) {
            return DropdownMenuItem(
              value: value,
              child: Text(
                value.name,
                style: TextStyle(fontSize: 18),
              ),
            );
          }).toList(),
        ),
      ),

      Padding(
          padding: const EdgeInsets.only(top: 30),
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              'Description',
              style: TextStyle(fontSize: 22),
            ),
          )),
      TextFormField(
        controller: descriptionInput,
        maxLength: 20,
        inputFormatters: [FilteringTextInputFormatter.allow(RegExp("[0-9a-zA-Z ]")),],
        decoration: InputDecoration(hintText: 'Expense description...'),
      ),

// Date
      Padding(
        padding: const EdgeInsets.only(top: 30),
        child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              'Date',
              style: TextStyle(fontSize: 22),
            )),
      ),

      TextField(
        controller: dateinput,
        decoration: InputDecoration(
            suffixIcon: Icon(Icons.calendar_today), hintText: "Date"),
        readOnly: true,
        onTap: () async {
          DateTime? pickedDate = await showDatePicker(
              context: context,
              initialDate: DateTime.now(),
              firstDate: DateTime(
                  2000), //DateTime.now() - not to allow to choose before today.
              lastDate: DateTime(2101));
          if (pickedDate != null) {
            String formattedDate =
                DateFormat.yMMMMd('en_US')
                    .format(pickedDate);
            String formatSendDate = DateFormat('yyyy-MM-dd').format(pickedDate);
            setState(() {
              dateinput.text =
                  formattedDate; //set output date to TextField value.
              sendDate = formatSendDate;
            });
          }
        },
      ),
      SizedBox(
        height: 50,
      ),
      TextButton(
        style: ButtonStyle(
          foregroundColor: MaterialStateProperty.all<Color>(
            Colors.white,
          ),
          backgroundColor: MaterialStateProperty.all<Color>(
              Theme.of(context).colorScheme.secondary),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                  side: BorderSide(
                      color: Theme.of(context).colorScheme.secondary))),
          padding: MaterialStateProperty.all<EdgeInsets>(
            EdgeInsets.only(top: 20, bottom: 20, right: 59, left: 59),
          ),
        ),
        onPressed: () async {
          if (amountInput.text == '' ||
              amountInput.text == '0' ||
              descriptionInput.text == '') {
            final snackBar = SnackBar(
              content: const Text(
                'Missing field!',
                textAlign: TextAlign.center,
              ),
            );
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          } else if (dropdownValue.balance<double.parse(amountInput.text.replaceAll('.', ''))){
            final snackBar = SnackBar(
              content: const Text(
                'Insufficient balance!',
                textAlign: TextAlign.center,
              ),
            );
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          } else {
            int tryAdd = await _addExpense(widget.userData.id, dropdownValue,
                    double.parse(amountInput.text.replaceAll('.', '')), descriptionInput.text)
                .then((value) => value);
            if (tryAdd == 1) {
              print('Add Expense Failed');
            } else {
              //pop -> mundur 1 module
              Navigator.pop(context);
              //balik jadi default sesudah pop
            }
          }
        },
        child: Text(
          'Save',
          style: TextStyle(fontSize: 22),
        ),
      ),
    ]);
  }
}
