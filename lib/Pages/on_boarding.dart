import 'package:flutter/material.dart';
import '/Model/content_model.dart';

import 'login.dart';

class OnBoarding extends StatefulWidget {
  const OnBoarding({Key? key}) : super(key: key);

  @override
  _OnBoardingState createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {
  int currentIndex = 0;
  PageController _controller = PageController();

  @override
  void initState() {
    _controller = PageController(initialPage: 0);
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          SafeArea(
            top: true,
            child: Container(
              padding: EdgeInsets.only(right: 20, left: 20),
              alignment: Alignment.centerRight,
              child: TextButton(
                child: Text(currentIndex == contents.length - 1 ? '' : 'Skip'),
                onPressed: currentIndex == contents.length - 1 ? null : () => _controller.animateToPage(
                        contents.length - 1,
                        duration: Duration(milliseconds: 300),
                        curve: Curves.linear,
                      ),
                style: TextButton.styleFrom(
                  primary: Theme.of(context).colorScheme.primary,
                  textStyle: TextStyle(
                    fontSize: 18,
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: PageView.builder(
                controller: _controller,
                itemCount: contents.length,
                onPageChanged: (int index) {
                  setState(() {
                    currentIndex = index;
                  });
                },
                itemBuilder: (_, i) {
                  return Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Column(
                      children: [
                        Spacer(),
                        // FlutterLogo(
                        //   //Nanti ganti sama gambar
                        //   size: 150,
                        // ),
                        Image.asset(contents[i].image, height: 200,),
                        // SvgPicture.asset("images/wallet_onboarding.png", height: 200,),
                        SizedBox(
                          height: 100,
                        ),
                        Text(
                          contents[i].title,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Text(
                          contents[i].description,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.grey,
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  );
                }),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: List.generate(
                  contents.length, (index) => buildDot(index, context)),
            ),
          ),
          SafeArea(
            child: Container(
              height: 60,
              margin: EdgeInsets.only(left: 20, right: 20, bottom: 30, top: 40),
              width: double.infinity,
              child: TextButton(
                child: Text(
                  currentIndex == contents.length - 1 ? 'Continue' : 'Next',
                  style: TextStyle(fontSize: 18),
                ),
                onPressed: () {
                  if (currentIndex == contents.length - 1) {
                    //Buat arahin ke login page
                    Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (_) => LoginPage(),
                        ));
                  }
                  _controller.nextPage(
                      duration: Duration(milliseconds: 300),
                      curve: Curves.linear);
                },
                style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(
                        Theme.of(context).colorScheme.primary),
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.white),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30.0),
                    ))),
              ),
            ),
          )
        ],
      ),
    );
  }

  Container buildDot(int index, BuildContext context) {
    return Container(
      height: 10,
      width: currentIndex == index ? 25 : 10,
      margin: EdgeInsets.only(right: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Theme.of(context).colorScheme.secondary,
      ),
    );
  }
}
