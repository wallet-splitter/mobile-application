class OnBoardingContent{
  String image;
  String title;
  String description;

  OnBoardingContent({required this.image, required this.title, required this.description});
}

List<OnBoardingContent> contents = [
  OnBoardingContent(
    image: 'images/wallet_onboarding.png',
    title: 'Welcome to Wallet Splitter',
    description: 'Treat your money like a boss.'
  ),
  OnBoardingContent(
    image: 'images/projection_onboarding.png',
    title: 'Effective Split Plan',
    description: 'Divide your money automatically.'
  ),
  OnBoardingContent(
    image: 'images/start_onboarding.png',
    title: 'Try our app now',
    description: 'Use our app to manage your finance now.'
  )
];