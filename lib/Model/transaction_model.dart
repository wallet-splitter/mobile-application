class TransactionModel{
  late String id;
  late String userId;
  late String walletFromId;
  late String walletToId;
  late double amount;
  late String description;
  late DateTime trxDate;
  late int isIncome;
  late int isExpense;
  late int isTransfer;

  TransactionModel(String id, String userId, String walletFromId, String walletToId, String amount, String description, String trxDate, String isIncome, String isExpense, String isTransfer){
    this.id = id;
    this.userId = userId;
    this.walletFromId = walletFromId;
    this.walletToId = walletToId;
    this.amount = double.parse(amount);
    this.description = description;
    this.trxDate = DateTime.parse(trxDate);
    this.isIncome = int.parse(isIncome);
    this.isExpense = int.parse(isExpense);
    this.isTransfer = int.parse(isTransfer);
  }
}