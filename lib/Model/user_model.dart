class UserModel{
  late String id;
  late String name;
  late String email;

  UserModel(String id, String? name, String? email){
    this.id=id;
    this.name=name!;
    this.email=email!;
  }
}