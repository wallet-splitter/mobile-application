class WalletModel{
  late String id;
  late String userId;
  late int icon;
  late String iconFam;
  late String name;
  late double balance;
  late int isMain;

  WalletModel(String id, String userId, String icon, String iconFam, String name, String balance, String isMain){
    this.id=id;
    this.userId=userId;
    this.icon=int.parse(icon);
    this.iconFam=iconFam;
    this.name=name;
    this.balance=double.parse(balance);
    this.isMain=int.parse(isMain);
  }
}