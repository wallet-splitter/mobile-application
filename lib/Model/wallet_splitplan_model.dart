class WalletSplitPlanModel{
  late String id;
  late String userId;
  late int icon;
  late String iconFam;
  late String name;
  late int isSplitPlan;
  late double percentage;

  WalletSplitPlanModel(String id, String userId, String icon, String iconFam, String name, String isSplitPlan, String percentage){
    this.id=id;
    this.userId=userId;
    this.icon=int.parse(icon);
    this.iconFam=iconFam;
    this.name=name;
    this.isSplitPlan=int.parse(isSplitPlan);
    this.percentage=double.parse(percentage);
  }
}