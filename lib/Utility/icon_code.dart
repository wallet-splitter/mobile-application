import 'package:flutter/material.dart';

IconData getIcon(int codePoint, String iconFamily){
  return IconData(codePoint, fontFamily: iconFamily, fontPackage: 'font_awesome_flutter');
}