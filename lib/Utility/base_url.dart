class BaseURL {
  //Android = 10.0.2.2
  //iOS = 192.168.0.107
  //Hosting = https://api-skripsi.000webhostapp.com
  //Hosting = https://ws.dratech.id

  // Localhost
  // static String baseUrl = "http://192.168.64.3/wallet-splitter-api/api";
  // // static String baseUrl = "https://api-skripsi.000webhostapp.com/api/";
  // static String login = "$baseUrl/login.php";
  // static String getUser = "$baseUrl/get-user.php";
  // // static String register = "$baseUrl/register.php";
  // static String getWallets = "$baseUrl/get-wallets.php";
  // static String addWallet = "$baseUrl/add-wallet.php";
  // static String editWallet = "$baseUrl/edit-wallet.php";
  // static String getWalletSingle = "$baseUrl/get-wallet-single.php";
  // static String getFavWallets = "$baseUrl/get-favorite-wallets.php";
  // static String updateFavWallets = "$baseUrl/update-favorite-wallet.php";
  // static String deleteWallet = "$baseUrl/delete-wallet.php";
  // static String getBalance = "$baseUrl/get-balance.php";
  // static String editUser = "$baseUrl/edit-user.php";
  // static String addTrx = "$baseUrl/add-transaction.php";
  // static String addSplitPlanTrx = "$baseUrl/add-splitplan-transaction.php";
  // static String getHistory = "$baseUrl/get-history.php";
  // static String getWalletHistory = "$baseUrl/get-wallet-history.php";
  // static String getSplitPlan = "$baseUrl/get-split-plan.php";
  // static String addSplitPlan = "$baseUrl/add-split-plan.php";
  // static String editSplitPlan = "$baseUrl/edit-split-plan.php";
  // static String resetSplitPlan = "$baseUrl/reset-split-plan.php";

  // Server
  static String baseUrl = "https://ws.dratech.id/api/";
  static String login = "$baseUrl/login";
  static String getUser = "$baseUrl/get-user";
  // static String register = "$baseUrl/register";
  static String getWallets = "$baseUrl/get-wallets";
  static String addWallet = "$baseUrl/add-wallet";
  static String editWallet = "$baseUrl/edit-wallet";
  static String getWalletSingle = "$baseUrl/get-wallet-single";
  static String getFavWallets = "$baseUrl/get-favorite-wallets";
  static String updateFavWallets = "$baseUrl/update-favorite-wallet";
  static String deleteWallet = "$baseUrl/delete-wallet";
  static String getBalance = "$baseUrl/get-balance";
  static String editUser = "$baseUrl/edit-user";
  static String addTrx = "$baseUrl/add-transaction";
  static String addSplitPlanTrx = "$baseUrl/add-splitplan-transaction";
  static String getHistory = "$baseUrl/get-history";
  static String getWalletHistory = "$baseUrl/get-wallet-history";
  static String getSplitPlan = "$baseUrl/get-split-plan";
  static String addSplitPlan = "$baseUrl/add-split-plan";
  static String editSplitPlan = "$baseUrl/edit-split-plan";
  static String resetSplitPlan = "$baseUrl/reset-split-plan";
}
