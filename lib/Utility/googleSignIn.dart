import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;

import 'base_url.dart';

class GoogleSignInProvider extends ChangeNotifier {
  final googleSignIn = GoogleSignIn();

  GoogleSignInAccount? _user;

  GoogleSignInAccount get user => _user!;

  Future googleLogin() async {
    try {
      final googleUser = await googleSignIn.signIn();
      if (googleUser == null) return;
      _user = googleUser;

      final googleAuth = await googleUser.authentication;

      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      await FirebaseAuth.instance.signInWithCredential(credential);

      //return data disimpen di value setelah then nanti value yang paling kanan buat di return
      //_user! => null safety
      int tryLogin = await login(_user!.email, _user!.displayName).then((value) => value);
      print("TRY LOGIN RESPONSE >> $tryLogin");
      if (tryLogin == 0) {
        print("User ${_user!.email} registered");
      } else if (tryLogin == 1) {
        print("Failed register user");
      } else if (tryLogin == 2) {
        print("User ${_user!.email} registered as new user");
      }
    } on Exception catch (e) {
      print(e.toString());
    }
    
    //balikin ke yg paling atas listener -> login.dart
    notifyListeners();
    print(_user!.displayName);
  }

  //future -> async karena hit api
  //function
  Future<int> login(String email, String? name) async {
    
    final response = await http.post(
      Uri.parse(BaseURL.login),
      body: jsonEncode(<String, dynamic>{
        "name": name,
        "email": email
      })
    );
    
    //ubah balikan api jadi map -> jsconDecode
    final data = jsonDecode(response.body);

    return data['error_schema']['error_code'];
  }

  //method (gaada return value)
  // Future<int> register(String email, String? name) async {
  //   print('EMAIL >> $email ; NAME >> $name');
  //   print("REGIS JALAN");
  //   final response = await http.post(
  //     Uri.parse(BaseURL.register),
  //     body: jsonEncode(<String, dynamic>{
  //       "name": name,
  //       "email": email
  //     })
  //   );

  //   print('REGISTER >> ${response.body}');
  //   final data = jsonDecode(response.body);
  //   return data['error_schema']['error_code'];
  // }

  Future logout() async {
    await googleSignIn.signOut();
    FirebaseAuth.instance.signOut();
  }
}
